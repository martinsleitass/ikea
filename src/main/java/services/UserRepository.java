package services;

import model.ikea.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by mleitas on 27.04.2015..
 */
@Stateless
public class UserRepository {
    @PersistenceContext( unitName = "ikeapu" )
    EntityManager em;

    public List<User> list(){
        return  em.createQuery("select u from User u", User.class).getResultList();
    }

    public void addUser(User user){
        em.persist(user);
    }

    public void updateUser(User user){
        em.merge(user);
    }
}
