package services;

import model.Country;
import model.Currency;
import model.ItemPrice;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mleitas on 31.03.2015..
 */
@Stateless
public class PriceService {
    private static final String URL_PLACEHOLDER = "http://www.ikea.com/:country1/:country2/catalog/products/:product/";

    private CloseableHttpClient httpClient;
    private CloseableHttpResponse httpResponse;
    private List<Currency> currencyList;

    public PriceService() {
        httpClient = HttpClients.createMinimal();
        currencyList = this.getExchangeRatesFromECB();
    }

    public List<ItemPrice> getPriceForItem(String productCode){
        List<ItemPrice> priceList = new ArrayList<>();
        String url = URL_PLACEHOLDER.replaceAll(":product", removeDots(productCode).trim());
        for(Country country : Country.values()){
            String countryUrl = new String(url);
            countryUrl = countryUrl.replaceAll(":country1", country.getId1());
            countryUrl = countryUrl.replaceAll(":country2", country.getId2());
            HttpGet httpGet = new HttpGet(countryUrl);
            try {
                httpResponse = httpClient.execute(httpGet);
                String content = EntityUtils.toString(httpResponse.getEntity());
                Document document = Jsoup.parse(content);
                Elements priceSpan = document.select("span#price1");
                for (Element element : priceSpan){
                    if(element.childNode(0) instanceof TextNode){
                        TextNode textNode = (TextNode) element.childNode(0);
                        ItemPrice itemPrice = new ItemPrice(productCode,country, textNode.text().trim(), 0);
                        setEurPrice(itemPrice);
                        itemPrice.setItemUrl(countryUrl);
                        priceList.add(itemPrice);
                    }
                }
                httpResponse.close();
            } catch (IOException e) {
                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "cannot execute get for country "+country, ExceptionUtils.getStackTrace(e)));
            }
        }
        return priceList;
    }

    private String removeDots(String s){
        return s.replaceAll("\\.", "");
    }

    private void setEurPrice(ItemPrice itemPrice){
        String priceNumber;
        Currency currency;
        String tmp;
        switch (itemPrice.getCountry()){
            case LITHUANIA:
                tmp  = itemPrice.getPriceTag().trim();
                priceNumber = tmp.substring(0, tmp.lastIndexOf('\u20ac')).replaceAll(",",".").trim();
                currency = getCurrency(Country.LITHUANIA);
                break;
            case POLAND:
                tmp = itemPrice.getPriceTag().trim();
                priceNumber = tmp.substring(0, tmp.lastIndexOf("PLN")).replaceAll(",", ".").trim();
                currency = getCurrency(Country.POLAND);
                break;
            case GERMANY:
                priceNumber = itemPrice.getPriceTag().trim();
                currency = getCurrency(Country.GERMANY);
                break;
            case SWEDEN:
                tmp = itemPrice.getPriceTag();
                priceNumber = tmp.substring(0, tmp.lastIndexOf("kr")).replaceAll(",", ".").trim();
                currency = getCurrency(Country.SWEDEN);
                break;
            case FINLAND:
                priceNumber = itemPrice.getPriceTag().replaceAll(",", ".").trim();
                currency = getCurrency(Country.FINLAND);
                break;
            case CZECH_REPUBLIC:
                priceNumber = itemPrice.getPriceTag().replaceAll("-", "0").replaceAll(",", ".").trim();
                currency = getCurrency(Country.CZECH_REPUBLIC);
                break;
            case BELGIUM:
                priceNumber = itemPrice.getPriceTag().replaceAll(",", ".").trim();
                currency = getCurrency(Country.BELGIUM);
                break;
            case LV_FURNI:
                tmp = itemPrice.getPriceTag().trim();
                priceNumber = tmp.substring(0, tmp.lastIndexOf("PLN")).replaceAll(",", ".").trim();
                currency = getCurrency(Country.LV_FURNI);
                break;
            case LV_iSTILS:
                tmp = itemPrice.getPriceTag().trim();
                priceNumber = tmp.substring(0, tmp.lastIndexOf("PLN")).replaceAll(",", ".").trim();
                currency = getCurrency(Country.LV_iSTILS);
                break;
            default: priceNumber = "-1.0";
                currency = new Currency("EUR", 1.0);
                break;
        }
        itemPrice.setPriceInEur(Double.parseDouble(priceNumber.replaceAll("[^.0-9]","")) / currency.getRate());
    }

    private List<Currency> getExchangeRatesFromECB(){
        String xmlUrl = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
        List<Currency> currencyList = new ArrayList<>();
        try {
            XMLReader saxReader = XMLReaderFactory.createXMLReader();
            DefaultHandler handler = new DefaultHandler() {
                public void startElement(String uri, String localName,
                                         String qName, Attributes attributes) {
                    if (localName.equals("Cube")) {
                        String currency = attributes.getValue("currency");
                        String rate = attributes.getValue("rate");
                        if (currency != null && rate != null) {
                            try {
                                currencyList.add(new Currency(currency, Double.parseDouble(rate)));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            };
            URL url = new URL(xmlUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            InputStream stream = conn.getInputStream();
            saxReader.setContentHandler(handler);
            saxReader.setErrorHandler(handler);
            saxReader.parse(new InputSource(stream));
            stream.close();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return currencyList;
    }

    private Currency getCurrency(Country country){
        String isoCode;
        switch (country){
            case SWEDEN: isoCode="SEK";break;
            case CZECH_REPUBLIC: isoCode="CZK"; break;
            case POLAND: isoCode="PLN";break;
            case LV_FURNI: return new Currency("LFR",3.448275862068966);
            case LV_iSTILS: return new Currency("LIS", 3.401360544217687);
            default:
                return new Currency("EUR", 1.0);
        }
        for (Currency currency : currencyList){
            if(currency.getCode().equalsIgnoreCase(isoCode))
                return currency;
        }
        return null;
    }
}
