package controllers;

import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.util.SocialAuthUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

@ManagedBean(name = "userSession")
@SessionScoped
public class UserSessionBean implements Serializable {
    private SocialAuthManager manager;
    private String            originalURL;
    private String            providerID;
    private Profile profile;

    public UserSessionBean() {  }

    public void socialConnect() throws Exception {
        // Put your keys and secrets from the providers here
        Properties props = System.getProperties();
        props.put("graph.facebook.com.consumer_key", "1566716096929311");
        props.put("graph.facebook.com.consumer_secret", "53dcfac011e2aeea3e26ef8b9d73fce8");
        // Define your custom permission if needed
        props.put("graph.facebook.com.custom_permissions", "publish_stream,email,user_birthday,user_location,offline_access");

        // Initiate required components
        SocialAuthConfig config = SocialAuthConfig.getDefault();
        config.load(props);
        manager = new SocialAuthManager();
        manager.setSocialAuthConfig(config);

        // 'successURL' is the page you'll be redirected to on successful login
        ExternalContext externalContext   = FacesContext.getCurrentInstance().getExternalContext();
        //String          successURL        = externalContext.getRequestContextPath() + "socialLoginSuccess.xhtml";
        String successURL = ((HttpServletRequest) externalContext.getRequest()).getRequestURI();
        HttpServletRequest request = ((HttpServletRequest) externalContext.getRequest());
        String file = externalContext.getRequestContextPath()+"/socialLoginSuccess.xhtml";//request.getRequestURI();
        if (request.getQueryString() != null) {
            file += '?' + request.getQueryString();
        }
        URL reconstructedURL = new URL(request.getScheme(),
                request.getServerName(),
                request.getServerPort(),
                file);
        successURL = reconstructedURL.toString();
        System.out.println("successURL: "+successURL);
        String          authenticationURL = manager.getAuthenticationUrl(providerID, successURL);
        FacesContext.getCurrentInstance().getExternalContext().redirect(authenticationURL);
    }

    public void pullUserInfo() {
        try {
            // Pull user's data from the provider
            ExternalContext    externalContext = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletRequest request         = (HttpServletRequest) externalContext.getRequest();
            Map map             = SocialAuthUtil.getRequestParametersMap(request);
            if (this.manager != null) {
                AuthProvider provider = manager.connect(map);
                this.profile          = provider.getUserProfile();

                // Do what you want with the data (e.g. persist to the database, etc.)
                System.out.println("User's Social profile: " + profile);

                // Redirect the user back to where they have been before logging in
                FacesContext.getCurrentInstance().getExternalContext().redirect(originalURL);

            } else FacesContext.getCurrentInstance().getExternalContext().redirect(externalContext.getRequestContextPath() + "home.xhtml");
        } catch (Exception ex) {
            System.out.println("UserSession - Exception: " + ex.toString());
        }
    }

    public void logOut() {
        try {
            // Disconnect from the provider
            manager.disconnectProvider(providerID);

            // Invalidate session
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletRequest request         = (HttpServletRequest) externalContext.getRequest();
            //this.invalidateSession(request);
            this.setProfile(null);
            externalContext.invalidateSession();

            // Redirect to home page
            FacesContext.getCurrentInstance().getExternalContext().redirect(externalContext.getRequestContextPath() + "/login.xhtml");
        } catch (IOException ex) {
            System.out.println("UserSessionBean - IOException: " + ex.toString());
        }
    }

    // Getters and Setters

    public SocialAuthManager getManager() {
        return manager;
    }

    public void setManager(SocialAuthManager manager) {
        this.manager = manager;
    }

    public String getOriginalURL() {
        return originalURL;
    }

    public void setOriginalURL(String originalURL) {
        System.out.println("setting original url: "+originalURL);
        this.originalURL = originalURL;
    }

    public String getProviderID() {
        return providerID;
    }

    public void setProviderID(String providerID) {
        this.providerID = providerID;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
