package controllers;

import model.ItemPrice;
import services.PriceService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mleitas on 31.03.2015..
 */
@ManagedBean
@RequestScoped
public class ProductPriceController implements Serializable{
    private String productCode;
    private List<ItemPrice> priceList = new ArrayList<>();

    @Inject
    private PriceService priceService;

    public void searchOnIkeaSite(){
        priceList = priceService.getPriceForItem(productCode);
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public List<ItemPrice> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<ItemPrice> priceList) {
        this.priceList = priceList;
    }
}
