package controllers;


import model.ikea.User;
import services.UserRepository;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * Created by mleitas on 27.04.2015..
 */
@ManagedBean
@ViewScoped
public class UserController implements Serializable {
    @Inject
    private UserRepository userRepository;

    private List<User> users;

    private User user = new User();


    @PostConstruct
    public void init( ){
        users = userRepository.list();
    }

    public void addUser(){
        userRepository.addUser(user);

        user = new User();
        init();
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
