package model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by mleitas on 31.03.2015..
 */
public class Currency {
    private String code;
    private double rate;

    public Currency() {
    }

    public Currency(String code, double rate) {

        this.code = code;
        this.rate = rate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
