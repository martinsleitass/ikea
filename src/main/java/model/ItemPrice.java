package model;

/**
 * Created by mleitas on 31.03.2015..
 */
public class ItemPrice {
    private String productCode;
    private Country country;
    private String priceTag;
    private double priceInEur;
    private String itemUrl;

    public ItemPrice(String productCode, Country country, String priceTag, double priceInEur) {
        this.productCode = productCode;
        this.country = country;
        this.priceTag = priceTag;
        this.priceInEur = priceInEur;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getPriceTag() {
        return priceTag;
    }

    public void setPriceTag(String priceTag) {
        this.priceTag = priceTag;
    }

    public double getPriceInEur() {
        return priceInEur;
    }

    public void setPriceInEur(double priceInEur) {
        this.priceInEur = priceInEur;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }
}
