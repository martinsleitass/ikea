package model;

/**
 * Created by mleitas on 31.03.2015..
 */
public enum Country {
    LITHUANIA("lt", "lt"), POLAND("pl","pl"), GERMANY("de","de"), SWEDEN("se","sv"), FINLAND("fi","fi"), CZECH_REPUBLIC("cz","cs"), BELGIUM("be","nl"), LV_FURNI("pl","pl"), LV_iSTILS("pl", "pl");
    private String id1;
    private String id2;

    private Country(String id1, String id2) {
        this.id1 = id1;
        this.id2 = id2;
    }

    public String getId1() {
        return id1;
    }

    public String getId2() {
        return id2;
    }
}
