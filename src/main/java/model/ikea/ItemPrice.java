package model.ikea;



import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by mleitas on 27.04.2015..
 */
@Entity
public class ItemPrice {
    @Id
    private int id;
    private double priceLocal;
    private double priceEur;
    private int amount;

    @ManyToOne
    private Item item;

    @ManyToOne
    private Country country;
}
