package model.ikea;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by mleitas on 27.04.2015..
 */
@Entity
public class Country {
    @Id
    private int id;

    private String name;
    private String code;
    private String currencyCode;

}
