package model.ikea;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by mleitas on 27.04.2015..
 */
@Entity
public class Item {

    @Id
    private int id;

    private String code;
    private String name;
    private String pictureLink;

    @ManyToOne
    private Cart cart;
}
