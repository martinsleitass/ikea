package model.ikea;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by mleitas on 27.04.2015..
 */
@Entity
public class Cart {
    @Id
    private int id;
    private String name;

    @ManyToOne
    private User user;
}
